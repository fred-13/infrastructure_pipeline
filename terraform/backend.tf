terraform {
  backend "http" {
    address="https://gitlab.com/api/v4/projects/30115920/terraform/state/iacstate"
    lock_address="https://gitlab.com/api/v4/projects/30115920/terraform/state/iacstate/lock"
    unlock_address="https://gitlab.com/api/v4/projects/30115920/terraform/state/iacstate/lock"
    username="fred-13"
    lock_method="POST"
    unlock_method="DELETE"
    retry_wait_min=5
  }
}
